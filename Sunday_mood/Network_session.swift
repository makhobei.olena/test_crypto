
import Foundation
class Network {
    
    static let shared = Network()
    
     let api_key = "27efd91bcb96ca51eae8521d538e1bac274127b6"
     let arrayOfCurrency = "BTC,ETH,LTC,XRP"
        //var  resourceURL = URL(string: "https://api.nomics.com/v1/currencies/ticker?key=\(api_key)&ids=\(arrayOfCurrency)&convert=USD" )
    var  resourceURL : URL? = nil
    
    func setCurrency(currency : String) {
        
        let resourceString  =  "https://api.nomics.com/v1/currencies/ticker?key=\(api_key)&ids=\(arrayOfCurrency)&convert=\(currency)"
        
        guard let  resourceURL = URL(string: resourceString) else {
            fatalError()}
        print(currency)
        
        self.resourceURL = resourceURL
    }
    
    
    
    func getData(completion: @escaping (Result<[CurrencysCrypto], CurrencysCryptoError>) -> Void) {
        
        guard let nonNullUrl = resourceURL else
        {
            return
        }
        let dataTask = URLSession.shared.dataTask(with: nonNullUrl){ data, _, _ in
            guard let jsonData = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            do
                        {
                            let decoder = JSONDecoder()
                            let currencyResponse = try decoder.decode([CurrencysCrypto].self, from: jsonData)
                            
                            completion(.success(currencyResponse))
                        }
            catch{
                completion(.failure(.canNotProccesData))
            }
        }
        dataTask.resume()
    }
    enum CurrencysCryptoError :Error
    {
        case noDataAvailable
        case canNotProccesData
    }
}


