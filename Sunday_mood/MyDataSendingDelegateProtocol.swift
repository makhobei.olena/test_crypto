//
//  MyDataSendingDelegateProtocol.swift
//  Sunday_mood
//
//  Created by Oleh Makhobei on 14.07.2021.
//

import Foundation
import UIKit

protocol MyDataSendingDelegateProtocol {
    func sendDataToFirstViewController(myData: String)
}
