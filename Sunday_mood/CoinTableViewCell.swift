//
//  CoinTableViewCell.swift
//  Sunday_mood
//
//  Created by Oleh Makhobei on 15.07.2021.
//

import UIKit

class CoinTableViewCell: UITableViewCell {

    @IBOutlet weak var cellLbl: UILabel!
    @IBOutlet weak var cellView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
   
        // Configure the view for the selected state
    }
    
}
