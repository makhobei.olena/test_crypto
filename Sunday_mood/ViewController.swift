

import UIKit

class ViewController: UIViewController,MyDataSendingDelegateProtocol{
    
    var alert :UIAlertController? = nil
    
    func sendDataToFirstViewController(myData: String) {
        worldCurrency = myData
    }
    
    @IBOutlet weak var cryptoTable: UITableView!
    
    var currencyArray = [CurrencysCrypto]()
    
    var worldCurrency = "USD" // by default currency is usd
    
    override func viewDidLoad() {
        
        
        //creating custom BarButtonItem for Choosing Currency:
        let ChooseCurrencyButton = UIBarButtonItem(title: "Choose Currency", style: UIBarButtonItem.Style.plain, target: self, action: #selector(ChooseCurrencyButtonItemTapped(_:)))
        self.navigationItem.rightBarButtonItem = ChooseCurrencyButton
         
        
        super.viewDidLoad()
        
        cryptoTable.dataSource = self
        cryptoTable.tableFooterView = UIView()
        
        let nibName = UINib(nibName: "CoinTableViewCell", bundle: nil)
        cryptoTable.register(nibName, forCellReuseIdentifier: "coinCell")
        
      
    }

    @objc func ChooseCurrencyButtonItemTapped(_ sender:UIBarButtonItem!)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
             viewController.delegate = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
     
        
        // show loading dialog
         alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert?.view.addSubview(loadingIndicator)
        present(alert!, animated: true, completion: nil)
        // loading dialog end
        
        reloaData()
        
    }
    
    func reloaData() {
        Network.shared.setCurrency(currency: worldCurrency)
        Network.shared.getData(completion: {result  in
            
            if let currencys = try?result.get(){
                DispatchQueue.main.async {
                    self.alert?.dismiss(animated: true, completion: nil)
                    self.currencyArray.removeAll() // clear array from previous results
                    for element in currencys{
                        self.currencyArray.append(element)
                    }
                   
                    
                    self.cryptoTable.reloadData()
                }
                
            }
        })
        
    }
    
    }
    

   extension ViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return currencyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "coinCell", for: indexPath) as! CoinTableViewCell
        let currencyCell = currencyArray[indexPath.row]
        let printedLBL = String( "1 \(currencyCell.id) = \(currencyCell.showTwoDigitsPrice(price: currencyCell.price)) in \(worldCurrency)")
        cell.cellLbl.text = printedLBL
        
       
        cell.cellView.layer.cornerRadius = 15
        return cell
    }
    
    
    }
    


