

import Foundation
import UIKit
class SecondViewController: UIViewController, UITableViewDelegate,UITableViewDataSource{
    
    @IBAction func done(sender: AnyObject){
        navigationController?.popViewController(animated: true)
            }
    
    var delegate: MyDataSendingDelegateProtocol? = nil
    
    @IBOutlet weak var currenciesTable: UITableView!
    
    
    var worldCurrencys = ["USD","EUR","JPY"]
    var viewControler:ViewController? = nil
    
    
    override func viewDidLoad() {
        
        currenciesTable.delegate =  self
        currenciesTable.dataSource = self
        
        let nibName = UINib(nibName: "CoinTableViewCell", bundle: nil)
        currenciesTable.register(nibName, forCellReuseIdentifier: "coinCell")
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "coinCell", for: indexPath) as!CoinTableViewCell
        let currencyCell = worldCurrencys[indexPath.row]
        cell.cellLbl?.text = currencyCell
        cell.cellView.layer.cornerRadius = 15
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return worldCurrencys.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedResult = worldCurrencys[indexPath.row]
        delegate?.sendDataToFirstViewController(myData: selectedResult)
        
        
    }
}

//    }
 

