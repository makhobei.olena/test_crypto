//
//  CurrencyValue.swift
//  TEST_crypto
//
//  Created by Olena Makhobei on 15.06.2021.
//

import Foundation
struct Info : Codable{
    let id : String
    let name : String
    let price : String
}
