//
//  ViewController.swift
//  TEST_crypto
//
//  Created by Oleh Makhobei on 14.06.2021.
//

import UIKit
import Foundation

class ViewController: UIViewController ,UIPickerViewDelegate, UIPickerViewDataSource{
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    let currenciesArray = ["EUR","YPY","USD"]
    
  
    @IBOutlet weak var lblEth: UILabel!
    
    @IBOutlet weak var lblBtc: UILabel!
    
    @IBOutlet weak var lblXrp: UILabel!
    
    @IBOutlet weak var lblLtc: UILabel!
   
    
    
    let API_KEY = "27efd91bcb96ca51eae8521d538e1bac274127b6"

    
    func getData (url:String){
        let url = URL(string: url)!
        let task = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            
            if let notNullJsonData = data{
                DispatchQueue.main.async { 
                    self.parseJson(json: notNullJsonData)
                    
                }
            }
        }
        task.resume()
        
    }
    
    
    
    func parseJson(json:Data){

        let jsonDecoder = JSONDecoder()
        
        if let infoArray = try? jsonDecoder.decode([Info].self, from: json) {
            
            for info in infoArray{
                if info.id == "ETH"{
                    lblEth.text = "1 ETH = \(info.price)"
                }else if info.id == "XRP"{
                    lblXrp.text = "1 XRP = \(info.price)"
                }else if info.id == "LTC"{
                    lblLtc.text = "1 LTC = \(info.price)"
                }else if info.id == "BTC"{
                    lblBtc.text = "1 BTC = \(info.price)"
                }
            }
        }
    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPicker()
        // initialy load information for USD
        let link = generateLink(currency: "USD")
        getData(url: link)

    }
    
    
    
    func setupPicker(){
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    //picker view methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       return currenciesArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let pickerRowTitle = currenciesArray[row]
        return pickerRowTitle
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let currency = currenciesArray[row]
        let link = generateLink(currency: currency)
        getData(url: link)
    }

    //pickerview methods
    
    func generateLink(currency:String) -> String {
        
        
        var link = "https://api.nomics.com/v1/currencies/ticker?key=27efd91bcb96ca51eae8521d538e1bac274127b6&fbclid=IwAR1_5oo8Pt0oGOYyC7kTpmXctBZQCTXIwUfA1QLz_6G5w5ZDi37pqCOx9Ik&ids=BTC,ETH,XRP,LTC&convert=\(currency)"
        
        return link
        
    }
    
}


